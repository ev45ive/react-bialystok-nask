import React, { Component } from "react";
import { Album } from "../models/Album";
import { musicSearch } from "../core/services";

export const MusicContext = React.createContext({
  search(query: string): void {
    throw "No Provider for MusicContext";
  },

  results: [] as Album[]
});

type State = {
  results: Album[];
};

export class MusicProvider extends Component<{}, State> {

  state: State = {
    results: [
      {
        id: "123",
        name: "Test 123",
        artists: [],
        images: [
          {
            url: "http://placekitten.com/300/300",
            height: 300,
            width: 300
          }
        ]
      }
    ]
  };

  search = (query: string) => {
    musicSearch.searchAlbums(query).then(results =>
      this.setState({
        results
      })
    );
  };

  render() {
    return (
      <MusicContext.Provider
        value={{
          results: this.state.results,
          search: this.search
        }}
      >{this.props.children}</MusicContext.Provider>
    );
  }
}
