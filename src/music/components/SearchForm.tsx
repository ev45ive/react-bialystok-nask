import React, { Component, ChangeEvent } from "react";

type Props = {
  onSearch(query: string): void;
};

export default class SearchForm extends Component<Props> {
  queryChange = (e: ChangeEvent<HTMLInputElement>) => {
    const query = e.target.value;

    clearTimeout(this.handler!);

    this.handler = setTimeout(() => {
      this.props.onSearch(query);
    }, 400);
  };

  handler?: number;

  inputRef = React.createRef<HTMLInputElement>();

  componentDidMount() {
    if (this.inputRef.current) {
      this.inputRef.current.focus();
    }
  }

  render() {
    return (
      <div>
        <div className="input-group">
          <input
            ref={this.inputRef}
            // autoFocus={true}
            type="text"
            className="form-control"
            onChange={this.queryChange}
          />
        </div>
      </div>
    );
  }
}

/*
// Formik
<Form value, onSubmit>
    <Field name="query"
</Form> */
