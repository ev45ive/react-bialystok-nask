import React, { Component } from "react";
import { Album } from "../../models/Album";

import styles from "./SearchResults.module.css";

// console.log(styles)

type Props = {
  results: Album[];
};

export default class SearchResults extends Component<Props> {
  render() {
    return (
      <div>
        <div className="card-group mt-3">
          {this.props.results.map(result => (
            <div className={"card " + styles.result} key={result.id}>
              <img src={result.images[0].url} alt="" className="card-img-top" />
              <div className="card-body">
                <h3 className="card-title">{result.name} </h3>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  }
}
