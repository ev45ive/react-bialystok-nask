import React from "react";
import { shallow } from "enzyme";
import SearchForm from "./SearchForm";

describe("SeachForm ", () => {
  fit("should debounce search every 400ms", () => {
    jest.useFakeTimers();
    const spy = jest.fn();
    const wrapper = shallow<SearchForm>(<SearchForm onSearch={spy} />);
    
    //Typing..
    wrapper.instance().queryChange({
      target: { value: "Bat" }
    } as any);
    jest.runTimersToTime(200);
    
    //Typing..
    wrapper.instance().queryChange({
      target: { value: "Batman" }
    } as any);
    
    // Thinking..
    jest.runTimersToTime(200);
    expect(spy).not.toHaveBeenCalled()
    
    // Search!
    jest.runTimersToTime(200);
    expect(spy).toHaveBeenCalledWith("Batman");
  });
});

export default undefined;
