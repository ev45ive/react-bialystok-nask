import React, { Component, StatelessComponent } from "react";
import SearchForm from "../components/SearchForm";
import SearchResults from "../components/SearchResults";
import { MusicContext } from "../MusicContext";

export const MusicSearchView: StatelessComponent = React.memo(() => (
  <div>
    <div className="row">
      <div className="col">
        <MusicContext.Consumer>
          {({ search }) => <SearchForm onSearch={search} />}
        </MusicContext.Consumer>
      </div>
    </div>
    <div className="row">
      <div className="col">
        <MusicContext.Consumer>
          {({ results }) => <SearchResults results={results} />}
        </MusicContext.Consumer>
      </div>
    </div>
  </div>
));

export default MusicSearchView;
