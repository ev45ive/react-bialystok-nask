import React, { Component } from "react";
import { Playlist } from "../../models/Playlist";

type Props = {
  items: Playlist[];
  selected?: Playlist;
  onSelect: (item: Playlist) => void;
};

export default class ItemsList extends Component<Props> {
  static defaultProps = {
    items: [],
    onSelect: () => {}
  };

  render() {
    const { selected, items } = this.props;
    return (
      <div>
        <div className="list-group">
          {items.map((item, index) => (
            <div
              className={`list-group-item ${
                selected && selected.id == item.id ? "active" : ""
              }`}
              key={item.id}
              onClick={() => this.select(item)}
            >
              {index + 1}. {item.name}
            </div>
          ))}
        </div>
      </div>
    );
  }
  select(item: Playlist) {
    this.props.onSelect(item);
  }
}
