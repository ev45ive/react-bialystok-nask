import React, { Component } from "react";
import { Playlist } from "../../models/Playlist";

type State = {
  playlist: Playlist;
};
type Props = {
  playlist: Playlist;
  onSave: (playlist: Playlist) => any;
  onCancel: () => any;
};

export default class PlaylistForm extends Component<Props, State> {
  state: State = {
    playlist: {
      id: 123,
      name: "React TOP20",
      favorite: true,
      color: "#ff00ff"
    }
  };

  static getDerivedStateFromProps(props: Props, state: State): Partial<State> {
    return {
      playlist:
        props.playlist.id == state.playlist.id ? state.playlist : props.playlist
    };
  }

  render() {
    const { playlist } = this.state;
    return (
      <div>
        <div className="form-group">
          <label>Name:</label>
          <input
            type="text"
            className="form-control"
            value={playlist.name}
            name="name"
            onChange={this.handleInput}
          />
          Remining: {100 - playlist.name.length}
        </div>

        <div className="form-group">
          <label>Favorite:</label>
          <input
            type="checkbox"
            checked={playlist.favorite}
            name="favorite"
            onChange={this.handleInput}
          />
        </div>

        <div className="form-group">
          <label>Color:</label>
          <input
            type="color"
            name="color"
            value={playlist.color}
            onChange={this.handleInput}
          />
        </div>
        <input type="button" value="Cancel" onClick={this.props.onCancel} />
        <input
          type="button"
          value="Save"
          onClick={() => this.props.onSave(this.state.playlist)}
        />
      </div>
    );
  }

  /* albo - Formik */
  handleInput = (event: React.FormEvent<HTMLInputElement>) => {
    const target = event.currentTarget;
    const type = target.type;
    const value = type == "checkbox" ? target.checked : target.value;

    this.setState(state => ({
      playlist: {
        ...state.playlist,
        [target.name]: value
      }
    }));
  };
}
