import React, { Component } from "react";
import { Playlist } from "../../models/Playlist";

const PlaylistDetails = (props: { playlist: Playlist; onEdit: () => any }) => {
  return (
    <div>
      <h3>Details</h3>
      <dl>
        <dt>Name</dt>
        <dd>{props.playlist.name}</dd>
        <dt>Favourite</dt>
        <dd>{props.playlist.favorite ? <b>Yes</b> : "No"}</dd>
        <dt>Color</dt>
        <dd
          style={{
            color: props.playlist.color,
            borderBottom: "1px solid black"
          }}
        >
          {props.playlist.color}
        </dd>
      </dl>
      <input type="button" value="Edit" onClick={() => props.onEdit()} />
    </div>
  );
};

export default PlaylistDetails;
