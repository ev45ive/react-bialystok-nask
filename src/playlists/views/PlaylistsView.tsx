import React, { Component } from "react";
import PlaylistDetails from "../components/PlaylistDetails";
import ItemsList from "../components/ItemsList";
import { Playlist } from "../../models/Playlist";
import PlaylistForm from "../components/PlaylistForm";

type State = {
  playlists: Playlist[];
  selected?: Playlist;
  mode: "edit" | "show";
};

export default class PlaylistsView extends Component<{}, State> {
  state: State = {
    mode: "show",
    playlists: [
      {
        id: 123,
        name: "React TOP20",
        favorite: true,
        color: "#ff00ff"
      },
      {
        id: 234,
        name: "React Hits",
        favorite: false,
        color: "#ffff00"
      },
      {
        id: 345,
        name: "React the BEst of",
        favorite: true,
        color: "#00ffff"
      }
    ]
  };

  select = (selected: Playlist) => {
    this.setState({
      selected: this.state.selected == selected ? undefined : selected
    });
  };

  render() {
    return (
      <div>
        <h3>Playlists</h3>

        <div className="row">
          <div className="col">
            <ItemsList
              selected={this.state.selected}
              onSelect={this.select}
              items={this.state.playlists}
            />
          </div>
          <div className="col">
            {this.state.selected ? (
              this.state.mode == "show" ? (
                <PlaylistDetails
                  onEdit={() => this.setState({ mode: "edit" })}
                  playlist={this.state.selected}
                />
              ) : (
                <PlaylistForm
                  playlist={this.state.selected}
                  onCancel={() => this.setState({ mode: "show" })}
                  onSave={this.save}
                />
              )
            ) : (
              <p>Please select playlist</p>
            )}
          </div>
        </div>
      </div>
    );
  }

  save = (playlist: Playlist) => {
    // Save to backend
    // Fetch updated list
    // Update selected
    this.setState({
      selected: playlist,
      playlists: this.state.playlists.map(p =>
        p.id == playlist.id ? playlist : p
      ),
      mode: "show"
    });
  };
}
