export interface Track {
  id: string;
  name: string;
  preview_url: string;
}
