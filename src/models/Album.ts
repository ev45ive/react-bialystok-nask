import { Track } from "./Track";

export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  artists: Artist[];
  images: Image[];
  tracks?: Track[];
}

export interface Artist extends Entity {}

export interface Image {
  height: number;
  url: string;
  width: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}

export interface PagingObject<T> {
  items: T[];
  limit?: number;
  offset?: number;
}
