import { Track } from "./Track";

export interface Playlist {
  id: number;
  name: string;
  favorite: boolean;
  color: string;
  tracks?: Track[];
}
