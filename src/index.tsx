import React, { StatelessComponent } from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { auth } from "./core/services";
auth

ReactDOM.render(<App />, document.getElementById('root'));

(window as any).React = React;
(window as any).ReactDOM = ReactDOM;

declare global {
  interface Window { MyNamespace: any; }
}

// React.createElement('div',{title:'', style:{}, className:''}, ' Ala ma kota')

// Stateless component
// const MyDiv: StatelessComponent<{ title: string; style: {} }> = props => (
//   <div title={props.title} style={props.style} className="placki">
//     Ala ma kota
//   </div>
// );

// ReactDOM.render(
//   <MyDiv title="test" style={{}} />,
//   document.getElementById("root")
// );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
