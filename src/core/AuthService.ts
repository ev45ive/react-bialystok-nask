type AuthConfig = {
  auth_url: string;
  client_id: string;
  response_type: string;
  redirect_uri: string;
};

export class AuthService {
  constructor(private config: AuthConfig) {}

  token: string | null = null;

  authorize() {
    const { auth_url, redirect_uri, response_type, client_id } = this.config;

    const redirect = `${auth_url}?redirect_uri=${redirect_uri}&response_type=${response_type}&client_id=${client_id}`;

    location.href = (redirect);
  }

  getToken() {
    if (location.hash && !this.token) {
      const match = location.hash.match(/#access_token=([^&]*)/);
      this.token = match && match[1];
      // location.hash = ''
    }
    if (!this.token) {
      this.authorize();
    }
    return this.token;
  }
}

// type Config = Record<
//   "auth_url" | "client_id" | "response_type" | "redirect_uri",
//   string
// >;
