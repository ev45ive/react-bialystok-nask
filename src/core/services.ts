import { AuthService } from "./AuthService";
import { MusicSearchService } from "./MusicSearchService";

export const auth = new AuthService({
  auth_url: "https://accounts.spotify.com/authorize",
  redirect_uri: "http://localhost:3000/",
  response_type: "token",
  client_id: "bf6df4ebb299446384dbabe57b265876"
});

auth.getToken();

export const musicSearch = new MusicSearchService(auth);
