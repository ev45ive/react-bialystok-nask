import axios from "axios";
import { AlbumsResponse } from "../models/Album";
import { AuthService } from "./AuthService";

export class MusicSearchService {
  constructor(
    private security: AuthService,
    private searchUrl = "https://api.spotify.com/v1/search"
  ) {}

  searchAlbums(query: string) {
    return axios
      .get<AlbumsResponse>(this.searchUrl, {
        params: {
          type: "album",
          q: query
        },
        headers: {
          Authorization: `Bearer ${this.security.getToken()}`
        }
      })
      .then(res => {
        return res.data.albums.items;
      })
      .catch(err => {
        return Promise.reject();
      });
  }
}
