import renderer from "react-test-renderer";
// import shallow from "react-test-renderer/shallow";
// shallow.createRenderer()
import UsersList from "./UsersList";
import React from "react";
import { User } from "../services/User";

const mockUsers = (): User[] => [
  {
    id: 123
  }
];

describe("UsersView", () => {
  it("shoud render", () => {
    expect(
      renderer.create(<UsersList users={[]} />).toJSON()
    ).toMatchSnapshot();
  });

  it("shoud render users", () => {
    const users = mockUsers();
    expect(
      renderer.create(<UsersList users={users} />).toJSON()
    ).toMatchSnapshot();
  });
});

export default undefined;
