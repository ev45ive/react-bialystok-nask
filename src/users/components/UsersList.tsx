// tsrs
import * as React from "react";
import { User } from "../services/User";

interface UsersListProps {
  users: User[];
}

const UsersList: React.FunctionComponent<UsersListProps> = props => {
  return (
    <div>
      UsersView
      {props.users.map(user => (
        <div key={user.id}>{user.name}</div>
      ))}
    </div>
  );
};

export default UsersList;
