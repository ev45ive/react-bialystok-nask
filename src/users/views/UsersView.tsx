import { FunctionComponent, useEffect, useState } from "react";
import UsersList from "../components/UsersList";
import React from "react";
import { usersService } from "../services/services";
import { User } from "../services/User";


interface UsersViewProps {}
export const UsersView: FunctionComponent<UsersViewProps> = props => {
  const [users, setUsers] = useState([] as User[]);
  
  useEffect(
    () => {
      usersService.getAll().then(setUsers);
      // return () => { // unsubscribe };
    } /* , [input] */
  ,[]);

  return (
    <div>
      <UsersList users={users} />
    </div>
  );
};

export default UsersView;
