import { mount } from "enzyme";
import React from "react";
import UsersList from "../components/UsersList";
import "jest-enzyme";
import { UsersView } from "./UsersView";
import { usersService } from "../services/services";
import { User } from "../services/User";
import {} from "react/";
describe("UsersView", () => {
  let getAllSpy: jest.SpyInstance<Promise<User[]>, []>;

  beforeEach(() => {
    getAllSpy = jest.spyOn(usersService, "getAll").mockResolvedValue([]);
  });

  it("renders empty users list", () => {
    const wrapper = mount(<UsersView />);
    const list = wrapper.find(UsersList);

    expect(list).toExist();
    expect(list.prop("users")).toEqual([]);
  });

  it("requests data from UsersService", () => {
    const wrapper = mount(<UsersView />);
    expect(getAllSpy).toHaveBeenCalled();
  });

  it("renders data from UsersService", () => {
    const users = [{ id: 123 }];
    // getAllSpy = jest.spyOn(usersService, "getAll");
    // getAllSpy.mockResolvedValue(users);
    getAllSpy.mockImplementation(() => {
      debugger;
      return Promise.resolve(users);
    });

    const wrapper = mount(
      <div>
        <UsersView />
      </div>
    );
    debugger
    expect(getAllSpy).toHaveBeenCalled();
    wrapper.update();
    const list = wrapper.find(UsersList);
    console.log(list.html())
    expect(list.prop("users")).toEqual(users);
  });
});

export default undefined;
