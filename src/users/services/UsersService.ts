import { User } from "./User";
import Axios from "axios";

export class UsersService {
  getAll() {
    return Axios.get<User[]>("/users").then(resp => {
      return resp.data;
    });
  }
}
