import { User } from "./User";
import axios, { AxiosResponse } from "axios";
import AxiosAdapter from "axios-mock-adapter";
import { UsersService } from "./UsersService";

const api = new AxiosAdapter(axios, {
  // delayResponse:0
});

describe("UsersService", () => {
  it("fetches users", () => {
    const users: User[] = [
      {
        id: 123
      }
    ];

    api.onGet("/users").reply(200, users);

    const service = new UsersService();

    return service.getAll().then((response: User[]) => {
      expect(response).toEqual(users);
    });
  });
});

export default undefined;
