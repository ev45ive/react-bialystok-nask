import React from "react";
import { render, mount, shallow, ReactWrapper, EnzymeAdapter } from "enzyme";
import { TaskList } from "./TaskList";
import { ListItem, Task } from "./ListItem";

describe("TaskList", () => {
  let tasks: Task[] = [];
  let wrapper: ReactWrapper<{}, {}>;

  beforeEach(() => {
    tasks = [
      { id: 123, title: "Task 123" },
      { id: 234, title: "Task 234" },
      { id: 345, title: "Task 345" }
    ];
    wrapper = mount(<TaskList tasks={tasks} />);
  });

  it("should render tasks", () => {
    expect(wrapper.find(ListItem).length).toEqual(tasks.length);
  });

  it("should render task names", () => {
    const texts = wrapper.find(ListItem).map(c => c.text());
    const titles = tasks.map(c => c.title);
    expect(texts).toEqual(titles);
  });

  it("should highlight selected item", () => {
    wrapper.setProps({ selected: tasks[1] });

    const expected = wrapper
      .find(ListItem)
      .filter({ task: tasks[1] })
      .get(0);
    const selected = wrapper.find({ active: true }).get(0);

    expect(expected).toEqual(selected);
  });

  it("should select item on click", () => {
    const spy = jest.fn().mockImplementation(() => {
      ///
    });
    // listen if was selected
    wrapper.setProps({
      onSelected: spy
    });

    // click element
    wrapper
      .find(ListItem)
      .filter({ task: tasks[1] })
      .simulate("click");

    expect(spy).toHaveBeenCalledWith(tasks[1]);
  });
});

export default undefined;
