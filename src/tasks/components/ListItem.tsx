import React, { StatelessComponent } from "react";

// import styles from "styled-components";
// export const ListItem = styles.div`
//   color: #333;
//   border: #999;
// `;
export const ListItem: StatelessComponent<{
  active?: boolean;
  task: Task;
  onSelected: (task: Task) => void;
}> = props => (
  <div
    className={"list-group-item " + (props.active ? "active" : "")}
    onClick={() => props.onSelected(props.task)}
  >
    {props.task.title}
  </div>
);
export type Task = {
  id: number;
  title: string;
};
