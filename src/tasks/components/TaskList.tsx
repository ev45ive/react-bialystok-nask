import React, { StatelessComponent } from "react";
import { Task, ListItem } from "./ListItem";

export const TaskList: StatelessComponent<{
  tasks: Task[];
  selected?: Task;
  onSelected?: (task: Task) => void;
}> = ({ tasks, selected, onSelected = () => {} }) => {
  return (
    <div>
      {tasks.map(task => (
        <ListItem
          active={task == selected}
          key={task.id}
          task={task}
          onSelected={onSelected}
        />
      ))}
    </div>
  );
};
// TaskList.defaultProps = {
//   onSelected: () => {}
// };
