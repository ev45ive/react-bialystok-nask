// npm i --save react-test-renderer @types/react-test-renderer

import renderer from "react-test-renderer";
import { ListItem, Task } from "./ListItem";
import React from "react";
import { shallow } from "enzyme";

describe("ListItem", () => {
  let task: Task;

  beforeEach(() => {
    task = { id: 123, title: "Test 123" };
  });

  it("renders item", () => {
    const snap = renderer
      .create(<ListItem task={task} active={false} onSelected={() => {}} />)
      .toJSON();

    expect(snap).toMatchSnapshot();
  });

  it("renders selected item", () => {
    const snap = renderer
      .create(<ListItem task={task} active={true} onSelected={() => {}} />)
      .toJSON();

    expect(snap).toMatchSnapshot();
  });

  it("should emit onSelected when clicked", () => {
    const spy = jest.fn();
    const wrapper = shallow(
      <ListItem task={task} active={false} onSelected={spy} />
    );

    wrapper.simulate("click");
    expect(spy).toHaveBeenCalledWith(task);
  });
});

export default undefined;
