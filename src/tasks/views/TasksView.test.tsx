import { shallow, ReactWrapper, ShallowWrapper } from "enzyme";
import React from "react";
import { TaskList } from "../components/TaskList";
import { Task } from "../components/ListItem";
import { TasksView } from "./TasksView";

describe("TasksView", () => {
  let wrapper: ShallowWrapper<
    {},
    {
      tasks: Task[];
      title: string;
    },
    TasksView
  >;

  beforeEach(() => {
    wrapper = shallow<TasksView>(<TasksView />, {});
  });

  it("renders task list", () => {
    const list = wrapper.find(TaskList);
    expect(list).toHaveLength(1);
  });

  it("renders add task form", () => {
    const form = wrapper.find("form");
    expect(form).toHaveLength(1);
  });

  it("form submits tasks", () => {
    const form = wrapper.find("form");
    wrapper.setState({ title: "TestTask" });

    const spy = spyOn(wrapper.instance(), "addTask");
    // wrapper.instance().addTask = jest.fn();
    wrapper.instance().forceUpdate();
    wrapper.update();

    // form.find("button").simulate("click");
    form.simulate("submit");
    // debugger;
    expect(wrapper.instance().addTask).toBeCalledWith("TestTask");
  });

  it("add created task to list", () => {
    expect(wrapper.state().tasks.filter(t => t.title == "Test")).toHaveLength(
      0
    );
    wrapper.instance().addTask("Test");

    expect(wrapper.state().tasks.filter(t => t.title == "Test")).toHaveLength(
      1
    );
  });

  it("clears form after adding task", () => {

    wrapper.setState({title:'Test'})
    wrapper.instance().addTask("Test");
    expect(wrapper.state('title')).toEqual('')
  });
});

export default undefined;
