import React, { Component } from "react";
import { TaskList } from "../components/TaskList";
import { Task } from "../components/ListItem";

type State = {
  tasks: Task[];
  selected?: Task;
  title: string;
};

export class TasksView extends Component<{}, State> {
  state: State = {
    tasks: [{ id: 123, title: "test" }],
    title: ""
  };
  inputRef = React.createRef<HTMLInputElement>();
  render() {
    return (
      <div>
        <TaskList tasks={this.state.tasks} selected={this.state.selected} />

        <form onSubmit={() => this.addTask(this.state.title)}>
          <input
            type="text"
            className="form-control"
            value={this.state.title}
            onChange={e => this.setState({ title: e.target.value })}
          />
          <button
            type="submit"
            //  onClick={() => this.addTask()}
          >
            Add
          </button>
        </form>
      </div>
    );
  }

  addTask = (title: string) => {
    // globalService.addTask()
    this.setState(state => ({
      tasks: [
        ...state.tasks,
        {
          id: Date.now(),
          title
        }
      ],
      title:''
    }));
  };
}
