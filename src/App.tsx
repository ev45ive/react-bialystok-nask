import React, { Component, Fragment, Suspense, useState } from "react";
// import logo from './logo.svg';
import "./App.css";
import { MusicProvider } from "./music/MusicContext";
import UsersView from "./users/views/UsersView";
// import 'bootstrap/dist/css/bootstrap.css'

const LazyMusicView = React.lazy(() => import("./music/views/MusicSearchView"));
LazyMusicView;

export const App = () => {
  const [show, setShow] = useState(false);

  return (
    <MusicProvider>
      <div className="App">
        <div className="container">
          <div className="row">
            <div className="col">
              <h1>Hello App</h1>
              <UsersView />
              <button onClick={() => setShow(true)}>Show</button>
              {show && (
                <Suspense fallback={<div>Loading...</div>}>
                  <LazyMusicView />
                </Suspense>
              )}
            </div>
          </div>
        </div>
      </div>
    </MusicProvider>
  );
};

export default App;
